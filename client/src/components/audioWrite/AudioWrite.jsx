import React, { useRef, useEffect} from 'react'

const AudioWrite = ({ isPlaying, src, onAudioEnded, volume}) => {
  const audioElem = useRef();

  useEffect(() => {
    if (isPlaying) {
      audioElem.current.play();
    } else {
      audioElem.current.pause();
    }
    if (audioElem.current) {
      audioElem.current.volume = volume;
    }
  }, [isPlaying, volume]);


  const handleAudioEnded = () => {
    onAudioEnded();
  };

  return (
    <div >
      <div >
        <audio src={src} type="audio/wav" ref={audioElem} id="audio_element" onEnded={handleAudioEnded} key={src}/>
      </div>
    </div>
  );
};

export default AudioWrite;
