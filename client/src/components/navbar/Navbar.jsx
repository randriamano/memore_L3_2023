import React from 'react'
import './navbar.css'
import logo from "../../assets/images/aze.png"

const Navbar = () => {
  return (
    <div>
      <div className="aisafa__navbar-links">
        <div className="aisafa__navbar-links_logo">
          <img src={logo} />
        </div>
      </div>
    </div>
  )
}

export default Navbar
