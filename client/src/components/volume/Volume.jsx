import React from 'react'
import './volume.css'
import Box from '@mui/material/Box'
import Stack from '@mui/material/Stack'
import Slider from '@mui/material/Slider'
import {MdVolumeUp} from "react-icons/md"
const Volume = ({onChange}) => {
    
const handleVolumeChange = (event, newValue) => {
    onChange(newValue /100);
};

  return (
    <div className='volume'>
      <Box sx={{ width: 20 }} className="box">
        <Stack sx={{ height: 300 }} spacing={1} direction="row">
          <Slider
            orientation="vertical"
            size="small"
            defaultValue={70}
            aria-label="Small"
            valueLabelDisplay="auto"
            onChange={handleVolumeChange}
            color="error"
          />
      
        </Stack>
      </Box>
      
      <MdVolumeUp color='#B7B7B7'size={26}/>
    </div>
  )
}

export default Volume