import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { Write } from '../../containers/index';
import VertabsCont from '../vertabs/VertabsCont';
import { styled } from '@mui/material/styles';

function CustomTabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 2 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

CustomTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const StyledTabs = styled((props) => (
  <Tabs
    {...props}
    TabIndicatorProps={{ children: <span className="MuiTabs-indicatorSpan" /> }}
  />
))({
  '& .MuiTabs-indicator': {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  '& .MuiTabs-indicatorSpan': {
    maxWidth: 200,
    width: '100%',
    backgroundColor: '#252B48',
  },
});

const StyledTab = styled((props) => <Tab {...props} />)(
  ({ theme }) => ({
    textTransform: 'none',
    fontSize: theme.typography.pxToRem(20),
    marginRight: theme.spacing(2),
    color: '#9E9FA5',
    '&.Mui-selected': {
      color: '#252B48',
    },
    '&.Mui-focusVisible': {
      backgroundColor: 'rgba(100, 95, 228, 0.32)',
    },
  }),
);

export default function BasicTabs() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };


  return (
    <Box sx={{ width: '100%' }} >
        <Box>
        <StyledTabs value={value} onChange={handleChange}  centered>
          <StyledTab label="Hiaino" {...a11yProps(0)} />
          <StyledTab label="Hanoratra" {...a11yProps(1)} />
        </StyledTabs>
      </Box>
      
      <CustomTabPanel value={value} index={0}>
        <VertabsCont />
      </CustomTabPanel>
      <CustomTabPanel value={value} index={1}>
        <Write />
      </CustomTabPanel>
    </Box>
  );
}