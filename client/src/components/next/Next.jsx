import React, { useState } from 'react'
import './next.css'
import {MdOutlineSkipNext} from "react-icons/md"


const Next = ({onClick}) => {
  const [iconColor, setIconColor] = useState('#B7B7B7');

  const handleHover = () => {
    setIconColor('#FFFF');
  };

  const handleLeave = () => {
    setIconColor('#B7B7B7');
  };

  return (
    <div
        className="voice__next"
        onMouseEnter={handleHover}
        onMouseLeave={handleLeave}
        onClick={ onClick }
      >
          <div className="voice__next-button">
            <MdOutlineSkipNext size={30} style={{ color: iconColor }} />
          </div>
          <span className='tooltiptext'>Manaraka</span>
    </div>
  );
};

export default Next;
