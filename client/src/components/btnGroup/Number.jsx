import React from 'react'
import './number.css'

const Number = ({ number, onClick, isSelected }) => {
  const buttonStyle = {
    'background-color': isSelected ? "#A2B4C0" : "white",
    width: isSelected ? '23px' : '20px',
    height: isSelected ? '23px' : '20px',
    boxShadow: isSelected ? '0 2px 10px rgba(0, 0, 0, 0.5)': 'none',
  };

  const pStyle = {
    color: isSelected ? "white" : "#707070",
  };
  return (
    <div className='voice__number' onClick={onClick} style={buttonStyle}>
          <p style={pStyle}>{number +1}</p> <br />
    </div>
  )
}

export default Number