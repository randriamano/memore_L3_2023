import React from 'react'
import Number from './Number'
import './btnGroup.css'


const BtnGroup = ({items, selectedButton, click}) => {
    
  return (
    <div className='btnGroup'>
      {items.map((item, index) => (
        <Number
          key={index}
          onClick={() => {
            click(index);
          }}
          number={index}
          isSelected={selectedButton === index}
        />
      ))}
    </div>
  )
}

export default BtnGroup