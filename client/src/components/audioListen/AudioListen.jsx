import React, { useRef, useEffect } from 'react'
import './audioListen.css'
import AudioSpectrum from 'react-audio-spectrum'


const AudioListen = ({isPlaying, src, onAudioEnded, volume}) => {

    const audioElem = useRef();
    useEffect(() => {
        if (isPlaying)
        {
            audioElem.current.play();
        }
        else{
            audioElem.current.pause();
        }
        if (audioElem.current) {
          audioElem.current.volume = volume;
        }
    }, [isPlaying, volume])

    const handleAudioEnded = () => {
      onAudioEnded();
    };
    

  return (
    <div className='voice__audio-comp'>
        <div className="voice__audio-container">
        <audio src={src} type="audio/wav" ref={audioElem} id='audio_element' onEnded={handleAudioEnded} key={src}/>
        
        <AudioSpectrum
        id="audio-canvas"
        height={140}
        width={900}
        audioId={'audio_element'}
        capColor={'red'}
        capHeight={0.5}
        meterWidth={1}
        meterCount={2000}
        meterColor={[
            {stop: 0, color: '#629FF4'},
            {stop: 0.5, color: '#F89096'},
            {stop: 1, color: '#B1B4E5'}
        ]}
        gap={10}
        />

      </div>
    </div>
  )
}

export default AudioListen 