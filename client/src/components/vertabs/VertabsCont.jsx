import React from 'react'
import './vertabsCont.css'
import { Vertabs } from '..'

const VertabsCont = () => {
  return (
    <div className='vertabs__container'>
      <Vertabs></Vertabs>
    </div>
  )
}

export default VertabsCont
