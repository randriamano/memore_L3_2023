import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { Listen } from '../../containers';
import { fiarahabana, fisaorana, fialanTsiny, manontanyVaovao, fanaovambeloma, famangiana } from './text';
import { styled } from '@mui/material/styles';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 4 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const StyledTabs = styled((props) => (
  <Tabs
    {...props}
    TabIndicatorProps={{ children: <span className="MuiTabs-indicatorSpan" /> }}
  />
))({
  '& .MuiTabs-indicator': {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  '& .MuiTabs-indicatorSpan': {
    maxWidth: 100,
    width: '100%',
    backgroundColor: '#252B48',
  },
});

const StyledTab = styled((props) => <Tab {...props} />)(
  ({ theme }) => ({
    textTransform: 'none',
    fontSize: theme.typography.pxToRem(16),
    marginRight: theme.spacing(2),
    color: '#9E9FA5',
    '&.Mui-selected': {
      color: '#252B48',
    },
    '&.Mui-focusVisible': {
      backgroundColor: '#252B48',
    },
  }),
);

export default function Vertabs() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };


  return (
    <div className="con">
      <Box
      sx={{ flexGrow: 2, bgcolor: 'transparent', display: 'flex', height: 610 }}
    >
      <StyledTabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        sx={{ borderRight: 1, borderColor: 'divider' }}
      >
        <StyledTab label="Fiarahabana" {...a11yProps(0)} />
        <StyledTab label="Fisaorana" {...a11yProps(1)} />
        <StyledTab label="Manontany vaovao" {...a11yProps(2)} />
        <StyledTab label="Fialan-tsiny" {...a11yProps(3)} />
        <StyledTab label="Fanaovam-beloma" {...a11yProps(4)} />
        <StyledTab label="Famangiana" {...a11yProps(5)} />

      </StyledTabs>
      <TabPanel value={value} index={0}>
        <Listen listenContent={fiarahabana}></Listen>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Listen listenContent={fisaorana}></Listen>
      </TabPanel>
      <TabPanel value={value} index={2}>
      <Listen listenContent={manontanyVaovao}></Listen>
      </TabPanel>
      <TabPanel value={value} index={3}>
      <Listen listenContent={fialanTsiny}></Listen>
      </TabPanel>
      <TabPanel value={value} index={4}>
      <Listen listenContent={fanaovambeloma}></Listen>
      </TabPanel>
      <TabPanel value={value} index={5}>
      <Listen listenContent={famangiana}></Listen>
      </TabPanel>
    </Box>
    </div>
    
  );
}