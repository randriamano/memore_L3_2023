import React from 'react'
import './card.css'


const Card = ({text, animation}) => {
  return (
    <div className={`voice__card ${animation}`} key={text}>
      <p >{text}</p>
    </div>
  )
}

export default Card