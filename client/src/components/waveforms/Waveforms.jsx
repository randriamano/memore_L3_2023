import React, { useRef, useEffect} from "react";
import WaveSurfer from "wavesurfer.js";

const Waveforms = ({ audioSrc, isPlaying}) => {
  const waveformRef = useRef(null);
  const wavesurfer = useRef(null);

  useEffect(() => {
    wavesurfer.current = WaveSurfer.create({
      container: waveformRef.current,
      waveColor: "rgb(200, 0, 200)",
      progressColor: "rgb(100, 0, 100)",
      responsive: true,
    });

    wavesurfer.current.load(audioSrc);

    return () => {
      wavesurfer.current.destroy();
    };
  }, [audioSrc]);

  useEffect(() => {
    if (wavesurfer.current) {
      if (isPlaying) {
        //wavesurfer.current.seekTo(0); // Réinitialiser la position de lecture
        wavesurfer.current.play();
      } else {
        wavesurfer.current.pause();
      }
    }
  }, [isPlaying]);

  return <div ref={waveformRef} />;
};

export default Waveforms;
