import React from 'react'
import './playWrite.css'
import { AiFillCaretRight, AiOutlinePause } from 'react-icons/ai'

const PlayWrite = ({ play, isPlaying, audio }) => {
  return (
    <div className={`play ${!audio ? "disabled" : ""}`}>
      <div className="play-button" onClick={play}>
        {isPlaying ? (
          <AiOutlinePause color="#fff" size={30} />
        ) : (
          <AiFillCaretRight color="#fff" size={30} />
        )}
      </div>
    </div>
  );
}

export default PlayWrite
