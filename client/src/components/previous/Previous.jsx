import React, {useState} from 'react'
import './previous.css'
import { MdOutlineSkipPrevious } from "react-icons/md"

const Previous = ({onClick}) => {
    const [iconColor, setIconColor] = useState('#B7B7B7');

    const handleHover = () => {
      setIconColor('#FFFF');
    };
  
    const handleLeave = () => {
      setIconColor('#B7B7B7');
    };
  
    return (
      <div
          className="voice__previous"
          onMouseEnter={handleHover}
          onMouseLeave={handleLeave}
          onClick={onClick}
        >
          <div className="voice__previous-button">
            <MdOutlineSkipPrevious size={30} style={{ color: iconColor }} />
          </div>

          <span className='tooltiptext'>Hiverina</span>
        </div>
    );
}

export default Previous