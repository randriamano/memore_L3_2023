import React from 'react'
import './playListen.css'
import { AiFillCaretRight, AiOutlinePause } from 'react-icons/ai'

const PlayListen = ({Click, isClicked, audio}) => {
  const handleClick = () => {
    if (audio) {
      Click();
    }
  };
  return (
    <div className={`voice__play ${!audio ? 'disabled' : ''}`} onClick={handleClick} >
        <div className='voice__play-button'>
            {
              isClicked ? (<AiOutlinePause color='#ff4f5e'size={36}/>) : (<AiFillCaretRight color='#59cbb7'size={36}/>)
            }
            
        </div>
    </div>
  )
}

export default PlayListen