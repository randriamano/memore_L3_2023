import React, { useState, useEffect } from "react";
import "./inside.css";
import { BasicTabs } from "../../components";
import { BiHome } from "react-icons/bi";
import { useNavigate } from 'react-router-dom';
import DotLoader from "react-spinners/FadeLoader";

const Inside = () => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);

  // Utilisation de useEffect pour gérer le chargement initial
  useEffect(() => {
    // Simulez une pause de 2 secondes avant la fin du chargement (remplacez par votre logique réelle)
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  }, []);

  const handleLinkClick = () => {
    // Simulez une pause de 2 secondes avant la redirection (remplacez par votre logique réelle)
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      navigate('/');
    }, 3000);
  };

  return (
    <div className="inside">
      <BiHome size={30} color='#252B48' className="backHome" onClick={handleLinkClick} />
      {loading ? (
        <div className="load-overlay">
        <div className="load-content">
          <DotLoader
            loading={loading}
            size={200}
            color="hsla(209, 71%, 40%, 1)"
          />
        </div>
      </div>
      ) : (
        
        <BasicTabs />
      )}
    </div>
  );
};

export default Inside;

