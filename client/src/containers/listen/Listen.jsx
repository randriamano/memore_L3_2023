import React from 'react'
import { useState, useEffect } from "react"
import axios from "axios"
import './listen.css'
import ClipLoader from "react-spinners/ClipLoader"
import {
    Card,
    PlayListen,
    Next,
    Previous,
    AudioListen,
    BtnGroup,
    Volume,
  } from "../../components"



const Listen = ({listenContent}) => {
  const [items, setItems] = useState(listenContent.trim().split('\n'));
      const [currentIndex, setCurrentIndex] = useState(0);
      const [isPlaying, setIsPlaying] = useState(false);
      const [audioSrc, setAudioSrc] = useState("");
      const [animation, setAnimation] = useState(null);
      const [selectedButton, setSelectedButton] = useState(0);
      const [volume, setVolume] = useState(0.7);
  
      
    
      const handleNext = () => {
        if (currentIndex < items.length - 1) {
          setAudioSrc("");
          setAnimation("slide-out-blurred-left");
          setTimeout(() => {
            setCurrentIndex(currentIndex + 1);
            setAnimation("slide-in-blurred-right");
            setSelectedButton(currentIndex + 1);
          }, 500);
          sendDataToFlsak();
        }
      };
    
      const handlePrev = () => {
        if (currentIndex > 0) {
          setAudioSrc("");
          setAnimation("slide-out-blurred-right");
          setTimeout(() => {
            setCurrentIndex(
              (prevIndex) => (prevIndex - 1 + items.length) % items.length
            );
            setAnimation("slide-in-blurred-left");
            setSelectedButton(currentIndex - 1);
          }, 500);
          sendDataToFlsak();
        }
      };

      const handleBulletClick = (index) => {
        setAudioSrc("");
        setAnimation("slide-out-bck-center");
        setSelectedButton(index);
        setTimeout(() => {
          setCurrentIndex(index);
          setAnimation("fade-in-bck");
        }, 500);
        sendDataToFlsak();
      };
    
      const handleButtonClick = () => {
        setIsPlaying(!isPlaying);
      };
    
    
      const handleAudioEnded = () => {
        setIsPlaying(false);
      };
    
      
    
      const handleVolumeChange = (newValue) => {
        setVolume(newValue);
      };


      const sendDataToFlsak = () => {
        return new Promise((resolve, reject) => {
          const currentValue = items[currentIndex];
          axios
            .post("http://localhost:5000/", { value: currentValue })
            .then(() => {
              resolve();
            })
            .catch((error) => {
              console.error(error);
              reject(error);
            });
        });
      };
      
    
      const fetchData = () => {
        return new Promise((resolve, reject) => {
          axios
            .get("http://localhost:5000/get_src")
            .then((response) => {
              const data = response.data;
              const audio = data.audio_src;
              setAudioSrc(audio);
              resolve();
            })
            .catch((error) => {
              console.error(error);
              reject(error);
            });
        });
      };
      
    
      useEffect(() => {
        sendDataToFlsak()
          .then(() => fetchData())
          .catch((error) => {
            console.error(error);
          });
      }, [currentIndex]);

      return (
        <div>
          <div className="listen">
            <div className="voice__center">
              <div className="voice_slider">
                <Card
                  key={items[currentIndex]}
                  text={items[currentIndex]}
                  animation={animation}
                />
              </div>
              <div className="voice__volume">
                <Volume onChange={handleVolumeChange} />
              </div>
              
            </div>

            <div className="voice__bullets">
                <BtnGroup
                  items={items}
                  selectedButton={selectedButton}
                  click={handleBulletClick}
                  
                />
              </div>
    
            <div className="voice__primaryButtons">
              
              <div className="voice__primaryButtons-containers">
                
                <Previous
                  onClick={() => {
                    handlePrev();
                    
                  }}
                />
                {
                  !audioSrc?
                  <ClipLoader color={'#D0021B'} loading={!audioSrc} size={80}/>
                  :
                  <PlayListen
                  Click={handleButtonClick}
                  isClicked={isPlaying}
                  audio={audioSrc}
                />
                }
                
                <Next
                  onClick={() => {
                    handleNext();
                  }}
                />
              </div>
    
              <AudioListen
                className="voice__audio"
                isPlaying={isPlaying}
                src={audioSrc}
                key={audioSrc}
                onAudioEnded={handleAudioEnded}
                volume={volume}
              />
              
            </div>
            
          </div>
        </div>
      );
}




export default Listen