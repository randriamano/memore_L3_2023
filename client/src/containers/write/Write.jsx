import React,  {useState, useRef }from 'react'
import axios from "axios"
import Box from '@mui/material/Box'
import { createTheme, ThemeProvider } from '@mui/material/styles'
import { PlayWrite, AudioWrite} from '../../components'
import robot from '../../assets/images/rob2.png'
import line1 from '../../assets/images/line1.png'
import line2 from '../../assets/images/line2.png'
import './write.css'
import Slider from '../../components/slider/Slider.js'
import DotLoader from "react-spinners/MoonLoader"
import {AiOutlineSend} from 'react-icons/ai'
import OutlinedInput from '@mui/material/OutlinedInput';
import IconButton from '@mui/material/IconButton';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';
import FormControl from '@mui/material/FormControl';


const Write = () => {
  const [percentage, setPercentage] = useState(0)
  const [name, setName] = useState("");
  const [audioSrc, setAudioSrc] = useState("");
  const [isPlaying, setIsPlaying] = useState(false);
  const [duration, setDuration] = useState(0);
  const [currentTime, setCurrentTime] = useState(0);
  const [loadingAudio, setLoadingAudio] = useState(false);

  const audioRef = useRef()


  const handleSubmit = (event) => {
    event.preventDefault();
    // Vérifier si le champ est vide
    if (!name.trim()) {
      alert("Le champ ne peut pas être vide. Veuillez le remplir.");
      return; // Arrêter la soumission si le champ est vide
    }
    setAudioSrc("");
    setLoadingAudio(true);
    
    sendDataToFlsak()
      .then(() => fetchData())
      .catch((error) => {
        console.error(error);
      });
  };
  
  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      
      handleSubmit(e);
    }
  };

  const sendDataToFlsak = () => {
    const currentValue = name;
    return axios.post("http://localhost:5000/", { value: currentValue })
      .catch((error) => {
        console.error(error);
      });
  };
  

  const fetchData = async () => {
    try {
      const response = await axios.get("http://localhost:5000/get_src");
      const data = response.data;
      const audio = data.audio_src;
      setAudioSrc(audio);
      setLoadingAudio(false);
    } catch (error) {
      console.error(error);
    }
  };


  const handleAudioEnded = () => {
    setIsPlaying(false);
  };

  const handleInputFocus = (e) => {
    //e.target.value = "";
   // setAudioSrc("");
    setLoadingAudio(false);
    
  }

  const onChange = (e) => {
    const audio = audioRef.current
    audio.currentTime = (audio.duration / 100) * e.target.value
    setPercentage(e.target.value)
  }

  const play = () => {
    const audio = audioRef.current
    audio.volume = 0.1

    if (!isPlaying) {
      setIsPlaying(true)
      audio.play()
    }

    if (isPlaying) {
      setIsPlaying(false)
      audio.pause()
    }
  }

  const getCurrDuration = (e) => {
    const percent = ((e.currentTarget.currentTime / e.currentTarget.duration) * 100).toFixed(2)
    const time = e.currentTarget.currentTime

    setPercentage(+percent)
    setCurrentTime(time.toFixed(2))
  }


  const theme = createTheme({
    palette: {
      neutral: {
        main: '#164B60',
        contrastText: '#fff',
      },
    },
  });
  

  return (
    <div className='write__main'>

      <div className="write">
      
      <div className="write__center">

        <div className="inputTxt">
          <img src={line1} className='l1' alt="lin1"/>

          <div className="in">
            <ThemeProvider theme={theme}>
              <Box
                component="form"
                sx={{
                  '& > :not(style)': { m: 1, width: '45ch'},
                }}
                noValidate
                autoComplete="off"
              >


                <FormControl  variant="outlined">
                  <InputLabel htmlFor="outlined-adornment-password" style={{color: "#164B60", fontFamily: 'Calibri'}} >Fehezan-teny ho vakiana.</InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-weight" 
                    label="Fehezan-teny ho vakiana." 
                    variant="outlined"
                    placeholder="Soraty..."
                    multiline
                    rows={3}
                    onChange={(e) => setName(e.target.value)}
                    onFocus={handleInputFocus}
                    color='neutral'
                    onKeyPress={handleKeyPress}
                    style={{ fontSize: '2rem' }}
                    endAdornment={
                      <div className='ico'>
                        <InputAdornment position="end-bottom">
                          <IconButton aria-label="fingerprint" color="neutral" onClick={handleSubmit}>
                            <AiOutlineSend size={25} color='#164B60'/>
                          </IconButton>
                        </InputAdornment>
                      </div>
                      
                    }
                    />
                </FormControl>


              </Box>
            </ThemeProvider>
          </div>

        </div>

      </div>



        <img src={robot} className='sp' alt="robot"/>

       
          <div >
            <AudioWrite
              
              isPlaying={isPlaying}
              src={audioSrc}
              key={audioSrc}
              onAudioEnded={handleAudioEnded}
              volume={0.7}
            />
          </div>

          <div className='app-container'>
            <div className="audio">
              {
                !audioSrc && loadingAudio ? <DotLoader color={'#D0021B'} loading={!audioSrc} size={50}/>
                :
                <div className='pi'>
                  <PlayWrite play={play} isPlaying={isPlaying} audio={audioSrc} className="play-btn"/>
                  <div >
                    <Slider percentage={percentage} onChange={onChange} duration={duration} currentTime={currentTime} />
                    <audio
                      ref={audioRef}
                      onTimeUpdate={getCurrDuration}
                      onLoadedData={(e) => {
                        setDuration(e.currentTarget.duration.toFixed(2))
                      }}
                      src={audioSrc}
                    ></audio>
                  </div>
                </div>
                
              } 
            </div>

            <img src={line2} className='l2' alt='line2'/>
          </div>
              
        
      </div> 
    </div>
  )
}

export default Write
