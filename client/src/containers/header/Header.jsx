import React from 'react'
import './header.css'
import { useNavigate } from 'react-router-dom'
import ai from "../../assets/images/head.png"
import Cir from "./lineFinale.json"
import Lottie from "lottie-react"
import Letter from "./letterBulle.json"


function Header({ setLoading }) {
  const navigate = useNavigate();
 

  const handleLinkClick = () => {
    setLoading(true);

    // Simulez une pause de 2 secondes avant la redirection (remplacez par votre logique réelle)
    setTimeout(() => {
      setLoading(false);
      navigate('/inside');
    }, 2000);
  };

  return (
    <>
        
            <div className="aisafa__header section__padding" id="home">
             <div className="aisafa__header-content">
          <h1 className="gradient__teny">Ndao hiaraka hianatra ny teny <strong className='txte'>Malagasy .</strong></h1>
          <p>Hiaino ny fomba fanonona ireo teny malagasy amin'ny alalan'ny intelligence artificielle.</p>
          <div className='aisafa__header-content__btn'>
            <button type="button slide"  onClick={handleLinkClick}>Hanomboka</button>
          </div>
            
        </div>

        <div className="aisafa__header-image">
          <img src={ai} alt='ai head' />
          <div className="animation__circuit">
            <Lottie animationData={Cir} style={{ width: "80%" }} />
            <Lottie animationData={Cir} style={{ width: "100%" }} />
          </div>
          <div className="animation__letter">
            <Lottie animationData={Letter} style={{ width: "100%" }} />
          </div>
        </div>
            </div>
            
        
        
       

        

    </>
    
  )
}

export default Header