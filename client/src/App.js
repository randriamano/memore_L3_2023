import React, { useState } from "react";
import "./App.css";
import { Navbar } from "./components";
import { Header } from "./containers";
import DotLoader from "react-spinners/FadeLoader";

const App = () => {
  const [loading, setLoading] = useState(false);

  return (
    <>
      {loading ? (
        <div className="load-overlay">
          <div className="load-content">
            <DotLoader
              loading={loading}
              size={200}
              color="hsla(209, 71%, 40%, 1)"
            />
          </div>
        </div>
      ) : (
        <div className="gradient__bg">
          <Navbar />
          <Header setLoading={setLoading} />
        </div>
      )}
    </>
  );
};

export default App;
