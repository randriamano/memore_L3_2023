import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { Inside } from "./containers";

import App from "./App";
import "./index.css";

const rootElement = document.getElementById("root");
const root = ReactDOM.createRoot(rootElement);
root.render(
  <React.StrictMode>
    <Router>
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/inside" element={<Inside />} />
        <Route path="*" element={<p>No page</p>} />
      </Routes>
    </Router>
  </React.StrictMode>
);
