from flask import Flask, request, jsonify
import anvil.server
from flask_cors import CORS


app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "http://localhost:3000"}})
receive_data = None

@app.after_request
def add_cors_headers(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type')
    response.headers.add('Access-Control-Allow-Methods', 'GET, POST')
    return response

@app.route("/", methods=["POST"])
def receive_data():
    global receive_data
    receive_data = request.json['value']
    print("ON A RECU CECI: ", receive_data)

    return 'Data received'

    

@app.route("/get_src", methods=['GET'])
def get_src():
    global receive_data

    if receive_data is None:
        return 'No data available'
    
    audio = anvil.server.call('end_to_end_infer', receive_data)
    audio_cut = audio[77:87]
    print("Received text: ", receive_data, "-----------------", audio_cut)
    
    return jsonify({"audio_src": audio, "audio_cut": audio_cut, "text": receive_data})


if __name__ == '__main__':
    anvil.server.connect("server_L2YJL6XUFNWLUBDWNIBYHCY2-N3YAPWT24R6UYBAR")
    app.run(debug=True)
